import allure
import pytest
from selenium import webdriver
from pageObjects.HomePage import HomePage
from constants import BASE_URL


def pytest_addoption(parser):
    parser.addoption(
        "--impl_wait", action="store", default=5
    )


@pytest.fixture
def setup(request):
    browser_name = request.param
    if browser_name == "Chrome":
        driver = webdriver.Chrome()
    elif browser_name == "Firefox":
        driver = webdriver.Firefox()
    else:
        raise ValueError(f"Неподдерживаемый браузер: {browser_name}")

    impl_wait = request.config.getoption("--impl_wait")
    driver.implicitly_wait(impl_wait)
    request.cls.driver = driver

    allure_title = f"Тест с браузером {browser_name}"
    allure.dynamic.title(allure_title)
    home_page = HomePage(driver)
    home_page.navigate_url(BASE_URL)

    yield home_page
    driver.quit()


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep


@allure.title("Скриншот ошибки по кейсу")
@pytest.fixture
def log_on_failure(request):
    yield
    item = request.node
    if item.rep_call.failed:
        take_screenshot(request.cls.driver, "screenshot_failed_test")


def take_screenshot(driver, name):
    allure.attach(driver.get_screenshot_as_png(), name=name, attachment_type=allure.attachment_type.PNG)
