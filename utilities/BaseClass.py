import allure
import pytest
from selenium.webdriver import ActionChains


@pytest.mark.usefixtures("setup", "log_on_failure")
class BaseClass:

    def check_url(self):
        with allure.step(f"Получаем URL страницы"):
            return self.driver.current_url

    def navigate_url(self, url):
        self.driver.get(url)

    def double_click(self, locator):
        with allure.step("Двойной клик элемента"):
            action = ActionChains(self.driver)
            action.double_click(self.driver.find_element(*locator)).perform()

    def right_click(self, locator):
        with allure.step("Правый клик элемента"):
            action = ActionChains(self.driver)
            action.context_click(self.driver.find_element(*locator)).perform()

    def move_to_el(self, el):
        with allure.step("Переходим к элементу"):
            self.driver.execute_script("arguments[0].scrollIntoView();", el)

    def scroll_to_top(self):
        with allure.step("Скролим до верха страницы"):
            self.driver.execute_script("window.scrollTo(0, 0);")

