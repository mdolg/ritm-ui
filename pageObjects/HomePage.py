import allure
from selenium.webdriver.common.by import By

from utilities.BaseClass import BaseClass


class HomePage(BaseClass):

    def __init__(self, driver):
        self.driver = driver

    elements_card = (By.CSS_SELECTOR, "div[class='category-cards'] div:nth-child(1)")

    def select_elements_card(self):
        with allure.step(f"Выбираем карточку Elements"):
            return self.driver.find_element(*HomePage.elements_card).click()
