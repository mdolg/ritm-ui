import allure
from selenium.webdriver.common.by import By
from utilities.BaseClass import BaseClass


class ElementsPage(BaseClass):

    def __init__(self, driver):
        self.driver = driver

    home_directory_li = (By.CSS_SELECTOR, "#tree-node ol li:nth-child(1)")
    home_directory_arrow = (By.CSS_SELECTOR, "#tree-node ol li span button")
    downloads_directory_li = (By.CSS_SELECTOR, "#tree-node ol ol li:nth-child(3)")
    downloads_directory_arrow = (By.CSS_SELECTOR, "#tree-node ol ol li:nth-child(3) span button")
    word_file_check_box = (By.XPATH, "//span[text()='Word File.doc']")
    word_file_check_box_svg = (
        By.CSS_SELECTOR, "#tree-node ol ol li:nth-child(3) ol li:nth-child(1) span label span:nth-child(2) svg")
    text_result = (By.CSS_SELECTOR, "div[id='result'] span[class='text-success']")

    double_click_me_button = (By.XPATH, "//button[text()='Double Click Me']")
    double_click_message = (By.CSS_SELECTOR, "p[id='doubleClickMessage']")

    right_click_me_button = (By.XPATH, "//button[text()='Right Click Me']")
    right_click_message = (By.CSS_SELECTOR, "p[id='rightClickMessage']")

    click_me_button = (By.XPATH, "//button[text()='Click Me']")
    click_message = (By.CSS_SELECTOR, "p[id='dynamicClickMessage']")

    def get_menu(self, el_id):
        with allure.step(f"Выбираем элемент меню с id={el_id}"):
            return self.driver.find_element(By.CSS_SELECTOR, f"li[id='item-{el_id}']")

    def select_home_directory_li(self):
        with allure.step(f"Выбираем директорию home"):
            return self.driver.find_element(*ElementsPage.home_directory_li)

    def select_home_directory_arrow(self):
        with allure.step(f"Нажимаем на стрелку возле директорию Home"):
            return self.driver.find_element(*ElementsPage.home_directory_arrow).click()

    def select_downloads_directory_li(self):
        with allure.step(f"Выбираем директорию Downloads"):
            return self.driver.find_element(*ElementsPage.downloads_directory_li)

    def select_downloads_directory_arrow(self):
        with allure.step(f"Нажимаем на стрелку возле директорию Downloads"):
            return self.driver.find_element(*ElementsPage.downloads_directory_arrow).click()

    def select_word_file_checkbox(self):
        with allure.step(f"Нажимаем на чек-бокс возле WordFile документа"):
            return self.driver.find_element(*ElementsPage.word_file_check_box).click()

    def select_word_file_checkbox_svg(self):
        with allure.step(f"Выбираем svg картинку чекбокса"):
            return self.driver.find_element(*ElementsPage.word_file_check_box_svg)

    def get_value_from_text_result(self):
        with allure.step(f"Получаем значение из поля с текстом после нажатия чек бокса"):
            return self.driver.find_element(*ElementsPage.text_result).text

    def select_double_click_message(self):
        with allure.step(f"Выбираем элемент поле с текстом после нажатия double click button"):
            return self.driver.find_element(*ElementsPage.double_click_message).text

    def select_right_click_message(self):
        with allure.step(f"Выбираем элемент поле с текстом после нажатия right click button"):
            return self.driver.find_element(*ElementsPage.right_click_message).text

    def press_click_button(self):
        with allure.step(f"Нажимаем на click button"):
            return self.driver.find_element(*ElementsPage.click_me_button).click()

    def select_click_message(self):
        with allure.step(f"Выбираем элемент поле с текстом после нажатия click button"):
            return self.driver.find_element(*ElementsPage.click_message).text
