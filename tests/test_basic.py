import allure
import pytest
from pageObjects.ElementsPage import ElementsPage
from utilities.BaseClass import BaseClass
from constants import BASE_URL
from testData.enums import BasicTestData


class TestBasic(BaseClass):

    @allure.story("Тест первой стори")
    @allure.description("Описание теста")
    @allure.severity(allure.severity_level.BLOCKER)
    @pytest.mark.parametrize("setup", BasicTestData.Browsers.value, indirect=True)
    def test_basic(self, setup):
        home_page = setup
        elements_page = ElementsPage(self.driver)
        with allure.step(
                f"Проверяем, что URL страницы совпадает с  {BASE_URL}"):
            assert home_page.check_url() == BASE_URL

        home_page.select_elements_card()
        with allure.step(
                f"Проверяем, что URL страницы совпадает с  {BASE_URL}{BasicTestData.Elements_page_url.value}"):
            assert home_page.check_url() == BASE_URL + BasicTestData.Elements_page_url.value

        elements_menu = elements_page.get_menu(BasicTestData.Check_box_menu_id)
        elements_menu.click()
        with allure.step(
                f"Проверяем, что URL страницы совпадает с  {BASE_URL}{BasicTestData.Checkbox_page_url.value}"):
            assert home_page.check_url() == BASE_URL + BasicTestData.Checkbox_page_url.value

        elements_page.select_home_directory_arrow()
        with allure.step(
                f"Проверяем, что Директория Home раскрыта"):
            assert BasicTestData.Expanded_class.value in elements_page.select_home_directory_li().get_attribute(
                'class')

        elements_page.select_downloads_directory_arrow()
        with allure.step(
                f"Проверяем, что Директория Download раскрыта"):
            assert BasicTestData.Expanded_class.value in elements_page.select_downloads_directory_li().get_attribute(
                'class')

        elements_page.select_word_file_checkbox()
        with allure.step(
                f"Проверяем, что чек-бокс корректно проставлен у Word File директории "):
            assert BasicTestData.Checkbox_checked.value in elements_page.select_word_file_checkbox_svg().get_attribute(
                'class')

        with allure.step(
                f"Проверяем, что текст совпадает с {BasicTestData.Text_word.value}"):
            assert elements_page.get_value_from_text_result() == BasicTestData.Text_word.value
