import allure
import pytest
from utilities.BaseClass import BaseClass
from constants import BASE_URL
from testData.enums import BasicTestData
from pageObjects.ElementsPage import ElementsPage


class TestSecond(BaseClass):

    @allure.story('Тест второй стори')
    @allure.description('Описание теста')
    @allure.severity(allure.severity_level.BLOCKER)
    @pytest.mark.parametrize("setup", BasicTestData.Browsers.value, indirect=True)
    def test_second(self, setup):
        home_page = setup
        elements_page = ElementsPage(self.driver)
        with allure.step(
                f"Проверяем, что URL страницы совпадает с  {BASE_URL}"):
            assert home_page.check_url() == BASE_URL, f"URL страницы не совпадает с {BASE_URL}"

        home_page.select_elements_card()
        with allure.step(
                f"Проверяем, что URL страницы совпадает с  {BASE_URL}{BasicTestData.Elements_page_url.value}"):
            assert home_page.check_url() == BASE_URL + BasicTestData.Elements_page_url.value, \
                f"URL страницы не совпадает с {BASE_URL + BasicTestData.Elements_page_url.value}"

        elements_menu = elements_page.get_menu(BasicTestData.Button_menu_id.value)
        self.move_to_el(elements_menu)
        elements_menu.click()
        with allure.step(
                f"Проверяем, что URL страницы совпадает с {BASE_URL}{BasicTestData.Buttons_page_url.value}"):
            assert elements_page.check_url() == BASE_URL + BasicTestData.Buttons_page_url.value, \
                f"URL страницы не совпадает с {BASE_URL + BasicTestData.Buttons_page_url.value}"

        self.scroll_to_top()
        self.double_click(elements_page.double_click_me_button)
        message_double = elements_page.select_double_click_message()
        assert message_double == BasicTestData.Double_click_me_text.value, \
            f"Текст в поле после нажатия кнопки double click me неверный, в поле текст =  {message_double}"

        self.right_click(elements_page.right_click_me_button)
        message_right = elements_page.select_right_click_message()
        assert message_right == BasicTestData.Right_click_me_text.value, \
            f"Текст в поле после нажатия кнопки right click me неверный, в поле текст =  {message_right}"

        elements_page.press_click_button()
        message = elements_page.select_click_message()
        assert message == BasicTestData.Click_me_text.value, \
            f"Текст в поле после нажатия кнопки click me неверный, в поле текст = {message}"
