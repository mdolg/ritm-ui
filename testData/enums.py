from enum import Enum


class BasicTestData(Enum):
    Browsers = ["Chrome", "Firefox"]
    Text_word = "wordFile"
    Elements_page_url = "elements"
    Checkbox_page_url = "checkbox"
    Buttons_page_url = "buttons"
    Expanded_class = "rct-node-expanded"
    Checkbox_checked = "check"
    Double_click_me_text = "You have done a double click"
    Right_click_me_text = "You have done a right click"
    Click_me_text = "You have done a dynamic click"
    Check_box_menu_id = 1
    Button_menu_id = 4
